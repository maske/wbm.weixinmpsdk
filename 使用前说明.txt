一个更方便，更快速的C#SDK，希望能帮助更多的朋友学习和使用

名称：我不忙-微信公众平台SDK(Wbm.WeixinmpSDK)
作者：xusion
Q 群：25844867 开发者之家
文档：http://wobumang.com

====================================================================
【体验说明】
* 直接使用VS打开Wbm.OAuthV2SDK.sln项目。或者在IIS建立web网站，指向Wbm.OAuthV2Demo目录。
* 按F5直接运行。
* 登陆，授权，获取用户资源

====================================================================
【目录结构】
* Wbm.WeixinmpDemo SDK演示文件
* Wbm.WeixinmpSDK  SDK核心文件

====================================================================
【使用流程】
1、根据需要求修改配置文件(Wbm.Weixinmp.config)。
2、验证认证信息缓存。(参考Default.aspx文件)
3、获取用户认证地址。(参考Login.aspx文件)
4、提交数据。(参考Post.aspx文件)
5、获取用户资源。(参考Default.aspx文件)













