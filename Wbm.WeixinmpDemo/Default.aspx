﻿<%@ Page Language="C#" AutoEventWireup="true" %>

<%@ Import Namespace="Wbm.WeixinmpSDK.OAuths" %>
<%@ Import Namespace="Wbm.WeixinmpSDK.Helpers" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>我不忙-微信公众平台演示</title>
    <link href="style.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <%--声明：
        本SDK独立项目的原因是微信公众平台SDK的使用者是管理员，并非普通用户。
        本Demo只实现了登录的简单功能，更多复杂的功能请根据自身需求进行增减。
        使用本SDK请勿脱离各平台官方API文档。
        如使用过程发生错误或不明白之处，请加入Q群：25844867 开发者之家
    --%>
    <div>
        <h2>
            我不忙-微信公众平台演示
        </h2>
        <p>
            我不忙文档：<a href="http://wobumang.com/afx">http://wobumang.com/afx</a>
            <br />
            微信文档：<a href="http://mp.weixin.qq.com/wiki" target="_blank">http://mp.weixin.qq.com/wiki</a>
        </p>
        <%--
            我不忙-微信公众平台SDK 使用流程：
            1、根据需要求修改配置文件(Wbm.Weixinmp.config)。
            2、验证认证信息缓存。(参考Default.aspx文件)
            3、获取用户认证地址。(参考Login.aspx文件)
            4、提交数据。(参考Post.aspx文件)
            5、获取用户资源。(参考Default.aspx文件)
        --%>
        <%            
            //获取当前已登录的协议
            var oauth = new Wbm.WeixinmpSDK.OAuths.Weixinmps.WeixinmpOAuth();
            if (oauth != null && oauth.HasCache)
            {//判断是否已登录
                string oauth_name = oauth.OAuthName;
                Response.Write("AccessToken：<br />");
                Response.Write(oauth.AccessToken);
        %>
        <p>
            <a href="Logout.aspx">退出</a>
        </p>
        <h4>
            演示 (当前登录：<%=oauth.OAuthDesc%>)</h4>
        <hr />
        <p>
            用户openid：<br />
            <%                    
                /*
                //获取json数据
                NameValueCollection paras = oauth.GetTokenParas();
                //第一个拉取的OPENID，不填默认从头开始拉取
                paras.Add("next_openid", string.Empty);
                string response = oauth.ApiByHttpPost("user_get", paras);
                //{"total":2,"count":2,"data":{"openid":["","OPENID1","OPENID2"]},"next_openid":"NEXT_OPENID"}                    
                var data = Wbm.WeixinmpSDK.Helpers.UtilHelper.ParseJson<Wbm.WeixinmpSDK.OAuths.Weixinmps.Models.WeixinMOpenIds>(response);
                Response.Write(response);
                */
                var next_openid = string.Empty;
                var user = MP.Weixin.Demo.Controllers.WeixinController.UserGet(next_openid);
                Response.Write(string.Join("<br />", user.data.openid));
            %>
        </p>
        <p>
            分组数据：<br />
            <%
                var group = MP.Weixin.Demo.Controllers.WeixinController.GroupsGet();
                foreach (var item in group)
                {
                    Response.Write(item.name + "|" + item.id);
                    Response.Write("<br />");
                }
            %>
        </p>
        <div>
            <strong>获取用户基本信息：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            普通用户openid:<br />
            <input type="text" name="openid" value="" /><br />
            <input type="hidden" name="act" value="user_info" />
            <input type="submit" value="查询" />
            </form>
        </div>
        <div>
            <strong>发送客服消息：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            普通用户openid:<br />
            <input type="text" name="openid" value="" /><br />
            文本消息内容：<br />
            <input type="text" name="text" value="" /><br />
            <input type="hidden" name="act" value="message_custom_send" />
            <input type="submit" value="发送" />
            </form>
        </div>
        <%--<div>
            <strong>添加用户分组：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            分组名称:<br />
            <input type="text" name="name" value="" /><br />
            <input type="hidden" name="act" value="groups_create" />
            <input type="submit" value="提交" />
            </form>
        </div>--%>
        <div>
            <strong>获取用户所在分组id：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            用户openid:<br />
            <input type="text" name="openid" value="" /><br />
            <input type="hidden" name="act" value="groups_getid" />
            <input type="submit" value="提交" />
            </form>
        </div>
        <div>
            <strong>修改分组：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            分组id:<br />
            <input type="text" name="id" value="" /><br />
            新分组名称:<br />
            <input type="text" name="name" value="" /><br />
            <input type="hidden" name="act" value="groups_update" />
            <input type="submit" value="提交" />
            </form>
        </div>
        <div>
            <strong>移动用户分组：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            用户openid:<br />
            <input type="text" name="openid" value="" /><br />
            分组id:<br />
            <input type="text" name="to_groupid" value="" /><br />
            <input type="hidden" name="act" value="groups_members_update" />
            <input type="submit" value="提交" />
            </form>
        </div>
        <div>
            <strong>上传图片：</strong>
            <br />
            <form method="post" action="Post.aspx" target="_blank">
            <img src="Upload.jpg" /><br />
            <input type="hidden" name="act" value="media_upload" />
            <input type="submit" value="提交" />
            </form>
        </div>
        <div style="height: 100px;">
        </div>
        <div class="clear">
        </div>
        <%}
            else
            { %>
        <div class="list">
            <%foreach (var item in Wbm.WeixinmpSDK.OAuthConfig.GetConfigOAuths())
              { %>
            <a href="login.aspx?oauth=<%=item.name %>" class="<%=item.name %>">
                <%=item.desc %></a>
            <%} %>
            <div class="clear">
            </div>
        </div>
        <%} %>
    </div>
</body>
</html>
