﻿<%@ Page Language="C#" AutoEventWireup="true" ValidateRequest="false" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>发送数据 - 我不忙-微信公众平台演示</title>
</head>
<body>
    <div>
        <h2>
            我不忙-微信公众平台演示
        </h2>
        <p>
            我不忙文档：<a href="http://wobumang.com/afx">http://wobumang.com/afx</a>
            <br />
            微信文档：<a href="http://mp.weixin.qq.com/wiki" target="_blank">http://mp.weixin.qq.com/wiki</a>
        </p>
        <p>
            注：如果遇到错误，请参照文档：<a href="http://mp.weixin.qq.com/wiki/index.php?title=%E5%85%A8%E5%B1%80%E8%BF%94%E5%9B%9E%E7%A0%81%E8%AF%B4%E6%98%8E"
                target="_blank">http://mp.weixin.qq.com/wiki/index.php?title=%E5%85%A8%E5%B1%80%E8%BF%94%E5%9B%9E%E7%A0%81%E8%AF%B4%E6%98%8E</a>
        </p>
        <p>
            <%     
                string act = Request["act"];

                if (act == "message_custom_send")
                {
                    //发送客服消息
                    string openid = Request["openid"];
                    string text = Request["text"];

                    if (string.IsNullOrEmpty(openid))
                    {
                        Response.Write("openid不允许为空");
                        Response.End();
                    }
                    if (string.IsNullOrEmpty(text))
                    {
                        Response.Write("内家不允许为空");
                        Response.End();
                    }

                    try
                    {
                        MP.Weixin.Demo.Controllers.WeixinController.MessageCustomSendText(openid, text);
                        Response.Write("发送成功");
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                }
                else if (act == "media_upload")
                {
                    try
                    {
                        var file = Server.MapPath("Upload.jpg");
                        var type = "image";
                        var data = MP.Weixin.Demo.Controllers.WeixinController.MediaUpload(type, file);
                        Response.Write("执行成功：");
                        Response.Write(data.media_id);
                        var token = MP.Weixin.Demo.Controllers.WeixinController.GetCurrentOAuth().AccessToken;
                        Response.Write(string.Format("<p><img src='http://file.api.weixin.qq.com/cgi-bin/media/get?access_token={0}&media_id={1}' /></p>", token, data.media_id));
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                }
                else if (act == "groups_create")
                {
                    try
                    {
                        string name = Request["name"];
                        var data = MP.Weixin.Demo.Controllers.WeixinController.GroupsCreate(name);
                        Response.Write("执行成功：");
                        Response.Write(data.group.name);
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                }
                else if (act == "groups_get")
                {

                    try
                    {
                        var data = MP.Weixin.Demo.Controllers.WeixinController.GroupsGet();
                        Response.Write("执行成功：");
                        Response.Write(data.Count());
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex.Message);
                    }
                }
                else if (act == "groups_getid")
                {
                    //查询用户所在分组
                    string openid = Request["openid"];

                    try
                    {
                        var data = MP.Weixin.Demo.Controllers.WeixinController.GroupsGetId(openid);
                        Response.Write("执行成功：");
                        Response.Write(data.groupid);

                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex);
                        Response.End();
                    }

                }
                else if (act == "groups_update")
                {
                    //修改分组名
                    string id = Request["id"];
                    string name = Request["name"];

                    try
                    {
                        MP.Weixin.Demo.Controllers.WeixinController.GroupsUpdate(id, name);
                        Response.Write("执行成功：");

                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex);
                        Response.End();
                    }

                }
                else if (act == "groups_members_update")
                {
                    //移动用户分组                    
                    string openid = Request["openid"];
                    string to_groupid = Request["to_groupid"];

                    try
                    {
                        MP.Weixin.Demo.Controllers.WeixinController.GroupsMembersUpdate(openid, to_groupid);
                        Response.Write("执行成功：");

                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex);
                        Response.End();
                    }
                }
                else if (act == "user_info")
                {
                    //获取用户基本信息
                    string openid = Request["openid"];
                    try
                    {
                        var data = MP.Weixin.Demo.Controllers.WeixinController.UserInfo(openid);
                        Response.Write("执行成功：");
                        Response.Write(data.nickname);

                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex);
                        Response.End();
                    }

                }
                else if (act == "user_get")
                {
                    //获取关注者列表
                    string next_openid = Request["next_openid"];
                    try
                    {
                        MP.Weixin.Demo.Controllers.WeixinController.UserGet(next_openid);
                        Response.Write("执行成功：");
                    }
                    catch (Exception ex)
                    {
                        Response.Write(ex);
                        Response.End();
                    }
                }
                else
                {
                    Response.Redirect("./");
                }

            %>
        </p>
    </div>
</body>
</html>
